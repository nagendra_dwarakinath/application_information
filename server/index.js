﻿/*

 node framework design
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   13th Nov 2013
 Purpose:        Base index for the app

 Supported By:   Veerabhadra Sajjan
 Reviewed By:    Himanshu Dhami

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)       Date (updated date)     Purpose (description)
 Bindu Sagar                14th Nov 2013           Added function as a root

 */

(function () {
    'use strict';

    /* variable declaration */
    var config = require('./config-debug');
    var winston = require('winston');
    var server = require('./server');

    winston.add(winston.transports.File, {
        filename: config.logger.api
    });

    winston.handleExceptions(new winston.transports.File({
        filename: config.logger.exception
    }));

}());