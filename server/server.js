﻿/*

 node framework design
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   13th Nov 2013
 Purpose:        server for the app

 Supported By:   Veerabhadra Sajjan
 Reviewed By:    Himanshu Dhami

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)       Date (updated date)     Purpose (description)

 */

/* variable declaration */
var express = require('express');
var app = express();
var port = 8080;
var fs = require('fs');
var route = require('./routes/route');
var AppHandler = require('./modules/applicationdetails/handlers/appHandler');
var expressLogFile = fs.createWriteStream('./logs/express.log', { flags: 'a' });

/* instance of a appModel handler */
var appHandler = new AppHandler();

/* app configuration */
app.configure(function () {

    app.use(express.logger({ stream: expressLogFile }));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        next();
    });
    app.use('/sampleData',express.static(__dirname + '/sampleData'));
    app.use(app.router);

    app.use(express.static(__dirname + '/public'));

});

/* instance of a appModel handler */
var handlers = { applicationDetails: appHandler};

route.setup(app, handlers);
app.listen(port);
console.log("Express server listening on port: ", port);

module.exports = app;