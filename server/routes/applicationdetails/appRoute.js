/*

 node framework design
 ----------------------
 Author:         Usha Kumari
 Created Date:   2nd Dec 2013
 Purpose:        Model for Recruitment Interviewing Process

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */
/*

 node framework design
 ----------------------
 Author:         Usha Kumari
 Created Date:   2nd Dec 2013
 Purpose:        Route file for Recruitment Interviewing Process

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */
'use strict';

/* node API routes for appModel */
function setup(app, handlers){
    app.post('/api/applicationdetails', handlers.applicationDetails.createApplicationDetails);

};

module.exports.setup = setup;