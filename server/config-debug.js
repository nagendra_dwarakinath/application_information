/*

 node framework design
 ----------------------
 Author:         Usha Kumari
 Created Date:   2nd Dec 2013
 Purpose:        Configuration file for app

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

/* assigning the default parameters for app */
module.exports = {
    "logger": {
        "api": "logs/api.log",
        "exception": "logs/exceptions.log"
    }
};