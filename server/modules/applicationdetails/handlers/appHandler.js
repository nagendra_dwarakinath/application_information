/*

 node framework design
 ----------------------
 Author:         Usha Kumari
 Created Date:   2nd Dec 2013
 Purpose:        Handler for Recruitment Interviewing Process

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var AppRepository = require('../repositories/appRepository');
var applicationRepository = new AppRepository();

var AppHandler = function() {

    this.getAll = handleGetApplicationRequest;
    this.save = handlerSaveApplicationDetailsRequest;
    this.update = handlerUpdateApplicationDetailsRequest;
    this.delete = handlerDeleteApplicationDetailsRequest
};

/* event definition for get all data */
function handleGetApplicationRequest(request, response) {

    applicationRepository.getAll()
        .then(
        function(promises) {

            console.log('success: ', 'data fetched');
            return response.json({ results:JSON.parse( promises ) });
        },
        function(err) {

            console.log('error: ' + err, 'pass');
            response.json(400, {error: error.message});
        }
    );
}

// On success should return status code 201 to notify the client the Application Details
// creation has been successful
// On error should return status code 400 and the error message
function handlerSaveApplicationDetailsRequest(request, response) {

    applicationRepository.save(request.body)
        .then(
            function (appDetails) {
                response.json(201, true);
            },
            function (error) {
                response.json(400, {
                    error: error.message
                });
            }
        );

}

function handlerUpdateApplicationDetailsRequest(request, response) {

    applicationRepository.update(request.body)
        .then(
        function (appDetails) {
            response.json(201, true);
        },
        function (error) {
            response.json(400, {
                error: error.message
            });
        }
    );
}

function handlerDeleteApplicationDetailsRequest(request, response) {
    applicationRepository.delete(request.params.Name)
        .then(
        function (appDetails) {
            response.json(201, true);
        },
        function (error) {
            response.json(400, {
                error: error.message
            });
        }
    );
}

module.exports = AppHandler;

