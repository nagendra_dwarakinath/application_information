/*
 node framework design
 ----------------------
 Author:         Usha Kumari
 Created Date:   2nd Dec 2013
 Purpose:        Recruitment Interviewing Process Repository

 Supported By:
 Reviewed By:
 */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/ApplicationDetails');
var AppModel = require('../../../model/appModel/appModel');
var Q = require('q');

function AppRepository() {

	this.save = saveData;
    this.getAll = getData;
    this.update = updateData;
    this.delete = deleteData;
}

/* function definition to save data */
function saveData(data) {

    var deferred = Q.defer();
    data = JSON.parse(JSON.stringify(data));
    console.log(data);

    var appDetails = new AppModel({
        Name: data.Name,
        Description: data.Description,
        Url: data.Url,
        Path: data.Path,
        IsActive: true
	});

    console.log("appDetails" + appDetails);
    appDetails.save(function(err, appDetails) {
		if (err) {
            console.log("err: " + err);
			deferred.reject(new Error(err));
		}
		else {
            console.log("pass: " + 'pass');
			deferred.resolve(appDetails);
		}
	});
	return deferred.promise;
}

/* function definition to get all data */
function getData() {

    var deferred = Q.defer();

    AppModel.find({}, function(err, data) {
        if (err) {
            console.log("err: " + err);
            deferred.reject(new Error(err));
        }
        else {
            console.log("pass: " + 'pass');
            deferred.resolve((JSON.stringify(data)));
        }
    });
    return deferred.promise;
}

/* function definition to update data */
function updateData(data) {
    var deferred = Q.defer();
    var query = {
        Name: data.Name
    };
    var options = {
        'new': true
    };
    AppModel.findOneAndUpdate(query,
        {
            Description: data.Description,
            Url: data.Url,
            Path: data.Path
        },
        options,
        function(err, appDetails) {
            if (err) {
                console.log("err: " + err);
                deferred.reject(new Error(err));
            }
            else {
                console.log("pass: " + 'pass');
                deferred.resolve(appDetails);
            }
        }
    );
    return deferred.promise;
}

/* function definition to delete data */

function deleteData(data) {
    var deferred = Q.defer();
    var query = {
        Name: data
    };
        AppModel.remove(query,function(err, appDetails) {
            if (err) {
             console.log("err: " + err);
             deferred.reject(new Error(err));
             }
             else {
             console.log("pass: " + 'pass');
             deferred.resolve(appDetails);
             }
        });
    return deferred.promise;
}
module.exports = AppRepository;
