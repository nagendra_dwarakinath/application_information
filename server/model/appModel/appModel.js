/*

 node framework design
 ----------------------
 Author:         Usha Kumari
 Created Date:   2nd Dec 2013
 Purpose:        Model for Recruitment Interviewing Process

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var mongoose = require('mongoose');

var appSchema = mongoose.Schema({

    Name: {type: String, required: true, index: {unique: true}},
    Description: {type: String, required: false},
    Url: {type: String, required: true},
    Path: {type: String, required: true},
    IsActive: {type: Boolean, required: true}
});

var AppModel = mongoose.model('appModel', appSchema);

module.exports = AppModel;
