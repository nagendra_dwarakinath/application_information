/*

 node framework design
 ----------------------
 Author:         Bindu Sagar
 Created Date:   14th Nov 2013
 Purpose:

 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            files: ['*.js', 'modules/**/*.js','test/**/*.js']
        },
        uglify: {
            options:{
                banner: '/*! <%= pkg.name %>' +
                    ' <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                src: 'modules/appModel/repositories/patientRepository.js',
                dest: 'deploy/modules/appModel/repositories/<%= pkg.name %>.<%= pkg.version %>.min.js'
            }
        },
        removelogging:{
            dist:{
                src: 'modules/appModel/repositories/patientRepository.js',
                dest: 'deploy/modules/appModel/repositories/patientRepository.min.js'
            }
        },
        cafemocha: {
            unitTests: {
                src: 'test/*/*.js',
                options: {
                    ui: 'bdd',
                    require: [
                        'should'
                    ],
                    reporter: 'spec'
                }
            }
        },
        mochacov: {
            options: {
                reporter: 'xunit-file',
                require: ['should'],
                bail: false
            },
            all: ['test/patientModule/appModel.js']
        },
        blanket_mocha: {
            all: [ 'specs/index.html' ],
            options: {
                threshold: 70
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-remove-logging');
    grunt.loadNpmTasks('grunt-cafe-mocha');
    grunt.loadNpmTasks('grunt-mocha-cov');
    grunt.loadNpmTasks('grunt-blanket-mocha');

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'uglify','removelogging','cafemocha','mochacov']);

};
