// dependencies
var express = require('express');
var http = require('http');

// setup HTTP server
var app = express();
var server = http.createServer(app);

// middleware for static requests
app.use(express.static(__dirname + '/app'));

// catch-all for get requests
app.get('*', function(req, res) {
  res.sendfile(__dirname + '/app/index.html');
});

// start listening for requests
server.listen(process.env.PORT || 1010);
console.log('Contender listening on port '+ server.address().port);
