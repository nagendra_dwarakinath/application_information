﻿/*

 angular js design
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   15th Nov 2013
 Purpose:        base application methods

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)       Date (updated date)     Purpose (description)
 
*/

'use strict';

(function () {

    /* get the api root url based on the path */
    app.api = {
        to: function (path) {

            return 'http://localhost:8080/api/' + path;
        }
    };

    app.isNullOrUndefined = function(value) {

        return (value === null || value === undefined) ? '' : value;
    };

    app.logout = function() {

        window.location = '/';
    };
    
    app.login = function () {

        window.location = '/';
    };
    
    app.goTo = function (tile) {

        window.location = '/' + tile;
    };
    
    /* Converts canvas to an image */
    app.convertCanvasToImage = function(canvas) {

        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        return image;
    };
    
    /* reset scroll bars */
    app.resetScrollBar = function (className, isVertical) {
        $(className).mCustomScrollbar("disable", true);

        if (isVertical) {
            setTimeout(function () {
                $(className).mCustomScrollbar({
                    autoHideScrollbar: true,
                    theme: 'dark-thick'
                });
            }, 150);
        } else {
            setTimeout(function () {
                $(className).mCustomScrollbar({
                    autoHideScrollbar: true,
                    horizontalScroll: true,
                    scrollButtons: {
                        enable: false
                    }
                });
            }, 150);
        }
    };
    
    app.ng.directive('qrcode', function () {
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {
                scope.$watch(attrs.data, function (value) {
                    if (value != undefined) {
                        $(element).empty();
                        $(element).qrcode({
                            //width: 250,
                            //height: 250,
                            text: value
                        });
                    }
                });
            }
        };
    });

    app.container = [{
        id: 1,
        name: "",
        rating: "",
        comment: "",
        interviewerRating: "",
        type: 'skills'
    },
       {
           id: 2,
           name: "",
           rating: "",
           comment: "",
           interviewerRating: "",
           type: 'jrssRating'
       }, {
           id: 3,
           name: "",
           rating: "",
           comment: "",
           interviewerRating: "",
           type: 'optionalSkills'
       }
    ];

    app.getContainer = function (type) {
        var data = [];
        for (var i = 0; i < app.container.length; i++) {
            if (type == app.container[i].type) {
                data.push(app.container[i]);
            }
        }
        return data;
    };

    app.resetContainer = function () {
        app.container = [{
            id: 1,
            name: "",
            rating: "",
            comment: "",
            interviewerRating: "",
            type: 'skills'
        },
            {
                id: 2,
                name: "",
                rating: "",
                comment: "",
                interviewerRating: "",
                type: 'jrssRating'
            }, {
                id: 3,
                name: "",
                rating: "",
                comment: "",
                interviewerRating: "",
                type: 'optionalSkills'
            }
        ];
    };

})();