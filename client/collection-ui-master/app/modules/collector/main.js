﻿'use strict';

(function () {

    /* controller definition */
    app.controller('collectorCtrl', ['$scope', '$http', function (scope, httpBackend) {

        /* variable declaration */
        scope.applications = [];
        scope.buttonValue="SAVE";
        /*scope.applications = [
            {
                name: 'Contender'
                , description: 'Means that the candidate has heard/read about the tool/technology/skill needed at work. Should be capable to build a conversation with the client about it.'
                , url: 'http://medseek-dev:8000'
            },
            {
                name: 'Data Parser'
                , description: 'Means that the candidate has heard/read about the tool/technology/skill needed at work. Should be capable to build a conversation with the client about it.'
                , url: 'http://medseek-dev:9090'
            },
            {
                name: 'ng Gallery'
                , description: 'Means that the candidate has heard/read about the tool/technology/skill needed at work. Should be capable to build a conversation with the client about it.'
                , url: 'http://medseek-dev:8080'
            },
            {
                name: 'code samples'
                , description: 'Means that the candidate has heard/read about the tool/technology/skill needed at work. Should be capable to build a conversation with the client about it.'
                , url: 'http://medseek-dev:9090'
            }
        ];*/

        /* reset data */
        scope.resetData = function () {
            scope.name = scope.description = scope.url = scope.path = '';
        };

        /* assign data */
        scope.assignData = function () {
            return {
                Name: scope.name,
                Description: scope.description,
                Url: scope.url,
                Path: scope.path
            };
        };

        /* load data for Edit */
        scope.populate = function(data)
        {
           scope.name = data.Name,
           scope.description = data.Description,
           scope.url = data.Url,
           scope.path = data.Path,
           scope.buttonValue = "EDIT"
        };


        scope.update = function () {
            var data = {
                "Name": scope.name,
                "Description": scope.description,
                "Url": scope.url,
                "Path": scope.path
            };
            httpBackend({
                    method: "PUT",
                    url: app.api.to('application'),
                    data: data,
                    contentType: "application/json;charset=utf-8"
                }
            ).success(function (result) {
                    debugger;
                    scope.resetData();
                    scope.load();
                    scope.buttonValue = "SAVE"
                    alert('Record updated successfully');
                }).error(function (error, status, headers, config) {
                    debugger;
                    alert('Error');
                });
        };


        scope.delete = function (application) {
        var test = application.Name;
            debugger;
            httpBackend({
                    method: "DELETE",
                    url: app.api.to('application/'+application.Name+''),
                    contentType: "application/json;charset=utf-8"
                }
            ).success(function (result) {
                    scope.resetData();
                    scope.load();
                    alert('Record deleted successfully');
                }).error(function (error, status, headers, config) {
                    debugger;
                    alert('Error');
                });
        };

        /* load data */
        scope.load = function() {

            httpBackend.get(app.api.to('application')).success(function (response) {
                var resultData;
                resultData = response.results || [];
                if (resultData.length)
                    scope.applications = response.results;
            });

        };

        /* save data */
        scope.save = function() {
            debugger;
        if(scope.buttonValue == "SAVE")
         {
            var data = JSON.stringify(scope.assignData());
            debugger;
            httpBackend({
                method: "POST",
                url: app.api.to('application'),
                data: data,
                contentType: "application/json;charset=utf-8"
            })
                .success(function (response) {
                    scope.resetData();
                    scope.load();
                    alert('Record Added Successfully');
                }).error(function (error, status, headers, config) {
                    debugger;
                    alert('Error in adding the data');
                });

        }
        else
        {
            scope.update();
        }
        };

    }]);

    app.publish('moduleReady', 'modules/collector');
})();