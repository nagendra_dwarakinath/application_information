﻿/*

 Medseek Generic Grid
 ----------------------
 Author:         Bindu Sagar Pasupuleti
 Created Date:   19th Nov 2013
 Purpose:        Medsek Generic Grid

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)       Date (updated date)     Purpose (description)
 
*/
(function () {

    app.ng.directive('medGrid', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'modules/controls/medGrid.html',
            scope: {
                type: '='
            },
            controller: function ($scope, $element, $attrs) {
                $scope.type = $attrs.type;
                $scope.header = $attrs.header;
                $scope.addRow = function (type) {
                    
                    app.container.push({
                        id: app.container.length + 1,
                        name: "",
                        rating: "",
                        comment: "",
                        type: type
                    });
                };
                $scope.deleteSearchCon = function (condition) {
                    
                    var index = 0;
                    for (var i = 0; i < app.container.length; i++) {
                        var id = app.container[i].id;
                        if (id == condition.id) {
                            break;
                        } else {
                            index = index + 1;
                        }
                    }
                    app.container.splice(index, 1);
                };
                $scope.getData = function (type) {
                    
                    var data = [];
                    for (var i = 0; i < app.container.length; i++) {
                        if (type == app.container[i].type) {
                            data.push(app.container[i]);
                        }
                    }
                    return data;
                };
                //$scope.jrss = $attrs.jrss;
                //$scope.optSkils = $attrs.optSkils;
            }
        };
    });

    /* allow only numeric data to enter */
    app.ng.directive('numeric', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelControl) {

                var numeric = function (inputValue) {
                    if (!app.isUndefinedOrNull(inputValue)) {
                        var number = inputValue.replace(/[^0-9]/g, '');

                        if (number != inputValue) {
                            ngModelControl.$setViewValue(number);
                            ngModelControl.$render();
                        }

                        return number;
                    }
                };

                ngModelControl.$parsers.push(numeric);
                numeric(scope[attrs.ngModel]);
            }
        };
    });
})();